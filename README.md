# Harbingers Of Exotic Transients

This repository contains the source files of my PhD thesis.

## Table of Contents
- The Story So  Far
    - Advanced LIGO and Advanced Virgo
    - The Time-Domain Sky
    - Organization Of Thesis

- Multi-messenger Astronomy With Gravitational-Waves
    - Compact Binary Coalescences
    - High Energy Astrophysics From Compact Objects
    - GW170817

- Low Latency Inference Of EM Counterparts From LIGO-Virgo
    - Motivation
    - Ellipsoid Based Inference
    - Machine Learning Based Inference

- Sky Tiling For The Zwicky Transient Facility
    - Tiling Strategies
    - Contour Covering vs. Ranked Tiling
    - Depth vs. Coverage

- Toward Rate Estimation For Transient Surveys
    - Motivation & Challenges
    - The intermediate Palomar Transient Factory
    - Single-epoch Efficiency
    - Type Ia Supernova
    - Other Transient Lightcurves
    - Rates

- Conclusion
    - Future Work
    - Looking Ahead
