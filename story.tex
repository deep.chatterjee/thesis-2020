In 1916 Albert Einstein published his relativistic theory of
gravity -- general relativity (GR) \citep{einstein_1916}. 
That light should bend around massive objects was the first
prediction of GR to be verified by Arthur Eddington and his team
\citep*{eddington_1919}. In 1916 Einstein also showed that GR allowed
wavelike solutions which he called gravitational waves. He
estimated the strength of these waves and noted that there
was no scope to detect them with the available technology at the
time. It took about 40 years for scientists to understand the
physical physical nature of gravitational waves. \cite{PhysRev.131.435}
first calculated the gravitational radiation from a binary star system.
The discovery of the binary pulsar PSR1913+16 by \citet{hulse_taylor}, and continued
observation of its orbital decay by \citet{taylor_weisberg} provided
compelling indirect evidence damping caused by gravitational-wave
emission as predicted by GR for a binary orbit. The 1993 Nobel
Prize in physics was awarded to Hulse and Taylor for this discovery.

Experimental efforts to directly measure gravitational waves
were first attempted using resonant mass bar detectors
by \citet{PhysRev.117.306}. Unfortunately, the sensitivity of these
detectors was far from what would be needed to capture astrophysical
sources. The idea of laser interferometry for this purpose was first
reported by \citet{1963JETP...16..433G}. In 1972 Rainer Weiss presented
an experimental design of an ``electromagnetically coupled broadband
gravitational antenna''~\footnote{\url{https://dcc.ligo.org/LIGO-P720002/public}}
that eventually led to the construction of the Laser Interferometer
Gravitational-wave Observatory (LIGO).\footnote{\url{https://dcc.ligo.org/LIGO-M890001/public}}
Initial LIGO started operations in the mid 2000s, with an upgrade
to Advanced LIGO commissioned in 2015. Almost after a century since the
initial prediction, gravitational waves from a binary black hole (BBH)
merger were directly observed by the twin LIGO detectors in 2015
\citep{GW150914-DETECTION}. The 2017 Nobel Prize in Physics was awarded
to Rainer Weiss, Kip Thorne, and Barry Barish for the inception of
LIGO and their contribution to the observation of gravitational waves.

\section{Advanced LIGO and Advanced Virgo}
\begin{figure}
    \includegraphics[width=0.32\textwidth]
        {figures/introduction/LIGO-Hanford.jpg}
    \includegraphics[width=0.32\textwidth, trim=0cm 6.3cm 0cm 3cm, clip]
        {figures/introduction/LIGO-Livingston.jpg}
    \includegraphics[width=0.32\textwidth, trim=0cm 1.1cm 0cm 0cm, clip]
        {figures/introduction/Virgo.jpg}
    \caption[Aerial views of the Advanced LIGO and Advanced Virgo detectors.]
    {Aerial views of the Advanced LIGO and Advanced Virgo detectors.
    Each of the LIGO detectors have 4 km long arms. The Virgo detector has
    3 km long arms. The long arms house a modified Michelson interferometer
    to detect gravitational waves. From left: LIGO Hanford, LIGO Livingston,
    and Virgo. Figures credits: \url{www.ligo.org} \& \url{www.virgo-gw.eu}.}
\end{figure}
The U.S. based, 4 km long, initial LIGO detectors were approved in the
1990s and finished construction over the early years of the turn
of the century. The French-Italian, 3 km long, initial Virgo detector was
also approved and constructed in a similar time frame. The initial LIGO
and Virgo detectors jointly analysed data between 2005--2007 in what is
known as the fifth science run (S5) for LIGO, and Virgo Scientific Run
(VSR1) for the Virgo detector \citep{Abadie_2010}.~\footnote{Previous
observing runs S1--S4 was performed only by the LIGO detectors.} The detectors
reached GW strain sensitivities $\sim 10^{-21}$, with LIGO at a distance
sensitivity of $\gtrsim 30$ Mpc for a pair of $1.4 M_{\odot}$ optimally
oriented binary neutron star (BNS) system. The Virgo detector reached
a horizon distance of $\simeq 8$ Mpc for the same. Expectedly, based on the
rate estimates of such binary mergers, no gravitational waves were detected
with the initial observatories. The detectors were upgraded to Advanced LIGO
(aLIGO) \citep{advanced_ligo} and Advanced Virgo (AdV), \citep{Acernese_2014}
with almost an order of magnitude improvement in overall strain sensitivity,
and better sensitivity to lower frequencies of GW inspiral at the start of
its observing runs.

\subsection{The First \& Second Observing Runs}
Advanced LIGO began its first observing run (O1) in September 2015.
On September 14, 2015 at 09:50:45 UTC, the twin LIGO detectors observed,
for the first time, GWs from a BBH merger at $\sim 400$ Mpc \citep{
GW150914-DETECTION}. This observing run saw two more stellar-mass BBH
mergers, GW151012 and GW151226. The AdV detector was in commissioning
period during this time.

The second observing run for aLIGO was conducted from November 30, 2016
to August 25, 2017. AdV started its observations in August 2017.
This observing run saw the detection of 10 confirmed BBH mergers. This was
a sharp increase since O1. It also saw the first ever gravitational-wave
detection from the coalescence of a BNS, GW170817 \citep{gw170817}. Electromagnetic
(EM) counterparts in the forms of gamma-rays (GRB 170817A), optical
and radio/X-ray (AT 2017gfo) afterglows were observed concordantly
from the source in a timeframe of seconds to months, even years after the
merger \citep{mma_2017, Mooley_2018, Hajela_2019}. The exhaustive observations
carried out across EM and high-energy spectrum were unprecedented. It bore
proof to the decade-long hypothesis that merging compact objects are progenitors
of high-energy astrophysical phenomena like GRBs and kilonovae \citep{lattimer_1974,
Li_1998}.

\subsection{2017: The Dawn Of Multi-messenger Astronomy}\label{sec:intro-mma}
The hypothesis that compact object collisions could create the environment
for launching high-energy astrophysical phenomena is not recent.
In 1974, \citeauthor{lattimer_1974} had proposed that NS-BH mergers had the
potential to tidally break the neutron star, and create the environment for
launching a short GRB. In 1998, \citeauthor{Li_1998} hypothesized a rapidly
evolving ($\sim$ 1 day) optical transient, a kilonova,\footnote{Originally
called a \emph{macronova}. The more modern term \emph{kilonova} was coined
by \cite{Metzger_2010}.} associated with merging binary neutron stars (BNS).
Almost four decades after the initial hypothesis, the detection of photons in
conjunction with gravitational waves (GWs) from GW170817 gave the first
observational evidence of the hypothesis.

Joint observations in astronomy have been performed before. A notable one
being the observation of neutrinos from the core-collapse supernova, SN
1987A, where the optical counterpart was observed a hew hours after the
detection of neutrinos by the Kamiokande--II and the Irvine--Michigan--Brookhaven
detectors on February 23, 1987. The follow-up operations for GW170817 was,
however, unprecedented. It was the first success story of the long effort
to jointly observe both GW and EM emission from astrophysical sources.
During O2, LIGO/Virgo had memoranda of understanding with 88 groups across
more than 20 countries to follow-up GW candidates. Infrastructure was set
up to exchange information over the Gamma-ray Coordinate Network (GCN)
\footnote{\url{https://gcn.gsfc.nasa.gov/}} to enable multimessenger observations
of astrophysical events. This included machine readable notices to provide
automated follow-up opportunities to robotic ground and space-based
facilities. Basic information about the discovered GW candidate,
like the inferred time of merger, participating instruments, sky
localization, probability of having a NS, or some remnant matter post
merger was provided with the notices. The O2 follow-up campaign was a
comprehensive effort in astronomy and astroparticle physics.

The participating facilities employed a variety of observing strategies.
All-sky searches were performed by high-energy instruments, and neutrino
detectors. These used spatio-temporal information about the GW candidate
to match internal databases about potential candidates near the trigger
time. Wide field surveys used tiling, and/or galaxy catalog targeted
searches based on the GW sky localization distributed with the notices.
Deep field/spectroscopy instruments pursued medium latency follow-up
operations post identification of candidates. Long term follow-up was
done in X-ray and radio. In case of GW170817, such operations continued
for over a year. A complete description of the O2 LIGO/Virgo EM follow-up
campaign is given in \cite{ligo_O2_emfollow_2019}. The discovery of
GW170817 heralded a new era in organized joint observational efforts
in EMGW astronomy. It showcased the rich science that is possible by
studying multiple messengers from the same source (see Sec.~\ref{sec:gw170817}
for a brief summary of the scientific impact). It gave the impetus to
the current efforts in multi-messenger astronomy.

\subsection{The Third Observing Run}
\begin{figure}[thp]
    \begin{center}
    \includegraphics[width=0.85\textwidth]
        {figures/introduction/H1L1V1-RANGE-O3A.png}
    \\
    \includegraphics[width=0.85\textwidth]
        {figures/introduction/H1L1V1-NETWORK_SEGMENTS-O3A.png}
    \end{center}
    \caption[Binary neutron star inspiral range of LIGO/Virgo in the third
    observing run.]{\textbf{Upper panel}: This figure shows the angle and volume
    averaged inspiral range for binary neutron star (BNS) systems for the first
    half of the LIGO/Virgo third observing run (O3). \textbf{Lower panel}:
    Coincident detector observation durations. Credits: LIGO detector characterization
    summary (\url{https://ldas-jobs.ligo.caltech.edu/~detchar/summary/}).}
    \label{fig:network-sensitivity}
\end{figure}
The LIGO/Virgo third observing (O3) run started on April 1, 2019. The observing
run came to an end on March 27, 2020.\footnote{Scheduled to end on April 30, 2020.
However, detector operations were suspended on March 27, 2020 due to ongoing COVID-19
pandemic situation.}
For the first time, the LIGO/Virgo candidate discoveries were broadcasted
publicly over the GCN.\footnote{\url{https://emfollow.docs.ligo.org/userguide/}}
Several improvements were made to the detectors during the break between
O2 and O3. The volume averaged BNS inspiral was $\sim 140$ Mpc for
L1, $\sim 120$ Mpc for H1, and $\sim 50$ Mpc for V1. A total of
{\numevents} GW candidates have been reported publicly. This is more than
a five-fold increase compared to events from O1 \& O2 combined. Also, the
duty cycle of the detectors have improved significantly with
at least two detectors being operational for $\gtrsim 80\%$ of this run
duration, and three detectors being so $\sim 50\%$ of the duration
(see Fig.~\ref{fig:network-sensitivity}). This produced
some extremely well localized candidates in O3.\footnote{
\url{https://gracedb.ligo.org/superevents/S200311bg/view/}
}~\footnote{\url{https://gracedb.ligo.org/superevents/S190814bv/view/}}
Additional data products were provided to aid follow up operations. Also,
the distribution of alerts, and their latency improved significantly with
the usage of new tools, and allocation of more resources for this purpose.

\section{The Time-Domain Sky}
\begin{figure}[htp]
    \begin{center}
    \includegraphics[width=0.9\textwidth, trim=0cm 0.5cm 0cm 1.5cm, clip]
        {figures/introduction/gw170817_notices.pdf}
    \end{center}
    \caption[GCN notice timeline related to GW170817 in the few
    hours post discovery.]{A timeline of GCN notices
    following the hours after the merger of the binary neutron star (BNS)
    system, GW170817, the discovery of concordant gamma-rays, GRB 170817A,
    and the optical and near infrared counterpart, AT 2017gfo.    
    }
\end{figure}
Simultaneous with the development of ground based GW detectors, the
last two decades have brought about a revolution in the field of time-domain
optical astronomy with experiments like Pan-STARRS, \citep{panstarrs1}
Sloan Digital Sky Survey, \citep{sdss} the ATLAS survey, \citep{atlas} the
Catalina survey, \citep{catalina} the All-Sky Automated Survey for Supernovae,
\citep{2019MNRAS.484.1899H} the Palomar and intermediate Transient Factory
(PTF), \citep{ptf} and Zwicky Transient Facility (ZTF) \citep{ztf_kulkarni}
performing all sky searches with rolling cadence to locate transients. Next
generation surveys like Vera Rubin Observatory \citep{lsst} are
expected to make significant additions to already existing catalogs with
wide-deep-fast searches. Current and upcoming telescope facilities are
consistent with the timeline of LIGO/Virgo operations, and plan to participate
in the follow-up efforts of GW sources (see \cite{Graham_2019}, for example).

The impact of such time-domain, robotic surveys have been significant in
discovering transients. Thousands of supernovae, for example, have been added
to the catalogs, and new classes of transients are been discovered. Among the
latter are fast radio bursts, fast X-ray transients, superluminous supernovae,
and kilonovae to name a few. Many such transients have compact object progenitors.
GWs can accompany, or be precursors of their progenitor dynamics. This goes
beyond the sources being observed by the ground based GW network today. Current
pulsar timing experiments,  like the North American Nanohertz Observatory
for Gravitational Waves (NANOGrav) \citep{jenet2009north}, will be able to
probe the regime of GW emission from supermassive BH inspirals. The Laser
Interferometer Gravitational-wave Antenna (LISA) \citep{amaroseoane2017laser}
will be able to probe their mergers. Future ground based detectors will be able
to see BNS mergers out to high redshifts ($z\gtrsim 1$). GW parameter estimation
along with photometric and spectroscopic data will be invaluable to infer the
high energy astrophysics of the compact object. High redshift multi-messenger
astronomy will be crucial for future cosmological observations. Therefore, the
synergy between EM and GW facilities is an exciting prospect in the coming decade.
%Population properties of such objects can be connected to galaxy properties
%such as star mass, age, star formation, and metallicity. Hence, it
%is important to study their rates and populations. 
\section{Organization Of Thesis}
We have reached an era when GW detection is routine. It is possible to
do astronomy with them. Joint detections requires robust infrastructure
to deliver candidate information and data products to external partners
to carry out follow-up observations. Along with better detectors, the
requirement of computing resources and cyber infrastructure is also
increasing. So is the need for new and efficient algorithms to handle
this stream of astrophysical data. Among others, the prospects of using
of machine learning is increasingly apparent to enable future discoveries.
In this thesis I present my work with the LIGO/Virgo collaborations and
the ZTF collaboration to allow multi-messenger astronomy.

The organization of the thesis is as follows. In Chapter~\ref{chapter:mma},
I discuss sources of GWs, and the principle of detecting them using
laser inteferometry. I briefly review some EM counterparts possible from
compact objects -- GRBs, supernovae and kilonovae. In Chapter~\ref{chapter:em-bright},
I give details about the low-latency source property estimation of LIGO/Virgo
compact binary coalescences (CBCs). The source properties provide a
realtime inference of the CBC having a counterpart and is a part of
the realtime notices circulated by LIGO/Virgo during the previous and
current operations. In Chapter~\ref{chapter:sky-tiling}, I give details
about a tiling strategy for the ZTF telescope used in the follow-up operations
of the GW sources. In Chapter~\ref{chapter:iptf-rates}, I give details about
characterizing the detection efficiency of transient surveys considering
the intermediate Palomar Transient Factory (iPTF) as a case study.
The detection efficiency is the missing piece in the determination of
transient rates from archival data. Conclusions and the roadmap ahead
is presented in Chapter~\ref{chapter:conclusion}.
