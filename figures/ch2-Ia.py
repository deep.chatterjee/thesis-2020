from math import exp
from astropy import constants as c, units as u
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad

def _integral(z, y):
    return 2*z*exp(-2*z*y + z**2)

def lam(x, y):
    integral_values = np.array([
        quad(_integral, 0, _x, args=(y,))[0] for _x in x
    ])
    return np.exp(-x**2)*integral_values

t_values = np.logspace(5, 7, 100)
tau_m = [2e6, 3e6, 5e6] 
tau_ni = 7.605e5
x_vals = [t_values/tau for tau in tau_m]
y_vals = [0.5*tau/tau_ni for tau in tau_m]

lam_vals = [lam(a, b) for a, b in zip(x_vals, y_vals)]
phi_t = [1e43*(10*l + np.exp(-(t_values/t_m)**2))
         for l, t_m in zip(lam_vals, tau_m)]

fig, ax = plt.subplots()
#plt.semilogx(t_values, phi_t, 'k--')
plt.ticklabel_format(axis="both", style="sci", scilimits=(0,0))
plt.grid(linestyle='--')
for phi, y, c in zip(phi_t, y_vals, ('r', 'b', 'k')):
    plt.plot(t_values, phi, c+'--',
             label='$\\tau_m/2\\tau_{\mathrm{Ni}} = %.2f$'%(y))
plt.xlabel('Time (s)')
plt.legend()
plt.ylabel('Luminosity (erg s$^{-1}$)')
ax.axvspan(1296000., 1468800., alpha=0.3, color='gray')
plt.savefig('luminosity-time-Ia.pdf')
