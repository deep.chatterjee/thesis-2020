The direct observation of GW in 2015 was a milestone in the field of
gravitation. It bore truth to the century old prediction made by Einstein,
and was another triumph of general relativity. It opened an entirely new
messenger to study the universe, analogous to Galileo using the telescope to
observe celestial bodies in the 1600s, pioneering modern day astronomy. The
field has moved in leaps and bounds over the (barely) five years since the
discovery. It can be said in some sense that we have moved from the era of GW
discovery to GW astronomy at the time of writing. The recently concluded
LIGO/Virgo third observing run reported $\numevents$ public candidates.
This is more than five-fold increase in the number of events from the first
and second observing runs combined and corresponds to detection of over 5
candidates per months on average during regular operating cycle. The detection
of GW170817, and the unprecedented follow-up bore the first observational
evidence that merger of compact binaries could have an associated EM counterpart.
This meant that GW astronomy entered the sector of time-domain astronomy.
Infrastructure was developed to enable rapid communication between GW
and partner facilities to enable prompt follow-up of GW transients.
This marked yet another milestone of the dawn of multi-messenger astronomy
using GWs. Targeted observations using GWs present a unique way to hunt
the rapidly evolving associated kilonova, GRB afterglows, and other
possible messengers. In this thesis, it is the prospect of multi-messenger
astronomy using GWs that I research on. In particular, I focus on development
of tools and infrastructure that facilitate follow-up operations for GW candidates
from LIGO/Virgo. In Chapter~\ref{chapter:em-bright} the inference of source properties
from the realtime GW data is developed to assess the possibility of EM counterparts
of compact object mergers. Such information is crucial for partner facilities
to schedule observations. In Chapter~\ref{chapter:sky-tiling}, the problem of
hunting these counterparts is considered from the point of view of partner
facilities. A tiling strategy is developed for ZTF in the pursuit
of GW events. Also, by performing a set of simulated signals, it is concluded that
longer integration times for the telescope increases chances of detecting
the kilonova. In Chapter~\ref{chapter:iptf-rates}, we study another piece
of the puzzle -- the detection efficiency of a survey. For a time-domain
survey this depends on intrinsic properties of the transient, its photometric
and spectral evolution, and also on weather conditions. A robust understanding
of the detection efficiency of fast transients like kilonovae is of
paramount importance to design observing strategies to succeed in hunting
EM counterparts of GW transients. While we are in the era of GW astronomy,
we are still not in an era of routine multi-messenger detections. In going
ahead tools and strategies like those developed in this thesis will be of
useful for follow-up operations.

\section{Future Work}
\subsection{Machine Learning}
Over the last decade machine learning has found importance in various avenues
of physics and astronomy. Extracting signal from noise, or dealing with
instrument or algorithm selection biases is common across most experiments
today. A robust handle on these selection effects require large scale expensive
computer simulations. Machine learning can help in this regard. Some of
the work in this thesis has made extensive use of traditional open source
machine learning algorithms to achieve good results. However, a natural extension
of work in Chapter~\ref{chapter:em-bright} is to use recently popularized
deep learning algorithms like convolutional neural networks to handle
selection effects.

\subsection{EoS Weighted Source Properties}
The source properties in Chapter~\ref{chapter:em-bright} used an assumption
about the NS EoS. To be conservative, a stiff EoS, 2H, was
chosen. The 2H is not astrophysically motivated, but was used to provide
a conservative answer to the property of the binary leaving remnant matter
post merger. This is because the detection uncertainty of realtime parameters
are much larger compared to difference caused due to the choice of an EoS.
This is to be reanalyzed in the light of new results from the third observing
run. A better approach is to consider several EoS models, or to marginalize
over the EoS by obtaining a weighted average of the results from using different
EoS models.

\subsection{Supernova Rates}
In Chapter~\ref{chapter:iptf-rates}, we evaluated the spacetime sensitive
volume of iPTF to SNe~Ia. The rate estimation was deferred to a future work.
An archival search is required to evaluate the number of candidates in iPTF
that fit the SALT2 model. The procedure is laid out in Sec.~\ref{sec:iptf-rates-rates}.
The archival search would give the efficiency and the impurity fraction of
the search to pick out SNe~Ia from archival data which is required to obtain
the posterior on the rate.

\section{Looking Ahead}
The next decade promises routine multi-messenger observations using GW
observations of compact binary mergers. Next generation telescope surveys
like ZTF and the Rubin Observatory will be able to make deep and fast routine
observations for transients. This will be complemented by $\sim 160 - 190$
Mpc sensitivity of LIGO/Virgo sensitivity in the fourth observing run.
The science output of GWs and multi-messenger observations is interdisciplinary,
touching the avenues of fundamental physics, nuclear physics and gravity,
to astronomy and cosmology. On the fundamental physics and gravity aspect,
the joint detection of EM counterparts and GWs provide constraints on the
speed of gravity, the future four-detector network will make good measurements
of the GW polarization to constrain alternate gravity models. Tidal deformability
measurements of GW will provide a direct method to measure the NS equation of
state. This will also be complemented by EM studies of the photometric and
spectroscopic evolution of the kilonova. Model selection in this regard would
require input from nuclear theory and lattice quantum chromodynamics models.
GWs in combination with counterpart observation will provide an independent
measurement of the Hubble constant, $H_0$. This is of paramount importance at
this point in cosmology given the growing tension in the inferred value of $H_0$
from the cosmic microwave background and type Ia supernovae. Future GW-GRB
coincidences will give more insight into the short GRB progenitors and the GRB
engine. There are also prospects of constraining dark matter using EMGW observations,
like the observation of primordial black holes which could produce binaries similar
to NS masses but don't produce EM counterparts. The growing number of observations
will put tight constraints on the population of compact binaries and their evolution
over cosmic time. Finally, there is always the possibility of detecting the unexpected
like an observation of binaries in the putative $3-5 \Msol$ mass-gap, objects
made out of more exotic matter like quark stars, or a totally unexpected EM
transient.