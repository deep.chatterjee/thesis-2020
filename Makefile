TARGET = main

DEPS = abstract.tex acknowledgements.tex story.tex mma.tex thesis-macros.tex em-bright.tex sky-tiling.tex rates.tex conclusion.tex appendix-rates.tex appendix-em-bright.tex

all: draft
	
draft: $(DEPS)
	xelatex -interaction=nonstopmode -synctex=1 $(TARGET) > /dev/null
	bibtex $(TARGET) > /dev/null
	xelatex -interaction=nonstopmode -synctex=1 $(TARGET) > /dev/null
	xelatex -interaction=nonstopmode -synctex=1 $(TARGET) > /dev/null

clean:
	/usr/bin/env rm -f *.dvi *.log *.out *.aux *.bbl *.blg *.toc *.bak *.lot *.lof *~ $(TARGET)Notes.bib
